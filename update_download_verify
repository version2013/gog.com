#!/bin/sh
# 2024 Jul 21
# 2024 Aug 08

# I recommend making a backup of manifest.dat and gog-resume-manifest.dat before running this script.

# run this command to extract game titles from manifest.dat :
# grep "'_title_mirror'" < gog-manifest.dat | sed "s/  '_title_mirror': '//" | sed "s/',//" > example.txt
#
# command also located: https://gitlab.com/version2013/gog.com/-/blob/main/extract_titles


if [[ ! "${1}" ]]; then
echo
echo Missing game title or file containing list of titles.
echo
echo I recommend making a backup of manifest.dat and gog-resume-manifest.dat before continuing.
echo
echo To get list of game titles from manifest.dat,
echo open this script as text, run command from line \8 \(without \#\)
echo
echo To \"update, download, verify\" one title,
echo run: ./update_download_verify game_title
echo for \"game_title\", use one game title from example.txt
echo 
echo To \"update, download, verify\" one or more titles,
echo run: ./update_download_verify example.txt -list
echo for \"example.txt\", use your text file that has as few or as many titles as you want \(one title per line\).
echo 
echo exiting...
exit 0
fi



if [[ "${2}" == "-list" ]]; then

while read -r title; do

echo
echo updating... "$title"
python gogrepoc.py update -ids "$title" -os windows linux -lang en -md5xmls

echo
echo downloading... "$title"
python gogrepoc.py download -ids "$title" -os windows linux -lang en -backgrounds -covers

echo | tee -a verify.log
echo verifying... "$title" | tee -a verify.log
python gogrepoc.py verify -forceverify -ids "$title" | tee -a verify.log

done < ${1}
echo exiting...
exit 0
fi



# options and descriptions from gogrepoc.py

# '-ids', 'id(s)/titles(s) of (a) specific game(s) to update'
# '-os', 'operating system(s)'
# '-lang', 'game language(s)'
# '-md5xmls' "Downloads the MD5 XML files for each item (where available) and outputs them to !md5_xmls"
echo
echo updating... ${1}
python gogrepoc.py update -ids ${1} -os windows linux -lang en -md5xmls

# -ids', 'id(s) or title(s) of the game in the manifest to download'
# '-os', 'download game files only for operating system(s)'
# '-lang', 'download game files only for language(s)'
# '-backgrounds', 'downloads background images for each game'
# '-covers', 'downloads cover images for each game'
echo
echo downloading... ${1}
python gogrepoc.py download -ids ${1} -os windows linux -lang en -backgrounds -covers

# '-ids', 'id(s) or title(s) of the game in the manifest to verify'
echo | tee -a verify.log
echo verifying... ${1} | tee -a verify.log
python gogrepoc.py verify -forceverify -ids ${1} | tee -a verify.log


echo exiting...
exit 0
