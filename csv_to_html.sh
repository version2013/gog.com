#!/bin/sh

# 2021 Oct 08
# 2021 Dec 14
#   removed asterisks (*) at end of archive.org urls

# https://www.linuxjournal.com/
# https://www.shellscript.sh/variables1.html
# https://www.grymoire.com/Unix/Sed.html
# https://www.unix.com/302071423-post3.html
# http://mywiki.wooledge.org/BashGuide
# http://mywiki.wooledge.org/BashFAQ/001
# https://www.cyberciti.biz/faq/unix-linux-bash-script-check-if-variable-is-empty/


while IFS="|" read -r date_source news_URL forum_URL title note; do
  date_formatted=$(echo $date_source > deleteme2/date_temp && date --file=deleteme2/date_temp +'%Y %b %d')
  short_URL=$(echo $news_URL | sed 's|https://www.gog.com/news/||')

if [ -z "$note" ]
then
      echo \ \ \ \ \ \ \ $date_formatted\ \?\ \<a href=\"$news_URL\"\>$title\</a\>\ \|\ \<a href=\"http://web.archive.org/web/*/$news_URL\"\>archive\<\/a\>\ \ \ \ \ \ \<a href=\"$forum_URL\"\>forum\<\/a\>\ \|\ \<a href=\"http://web.archive.org/web/*/$forum_URL\"\>archive\<\/a\> >> deleteme2/result
else
      echo [\<a href=\"\#$note\"\>$note\</a\>]\ \ \ \ $date_formatted\ \?\ \<a href=\"$news_URL\"\>$title\</a\>\ \|\ \<a href=\"http://web.archive.org/web/*/$news_URL\"\>archive\<\/a\>\ \ \ \ \ \ \<a href=\"$forum_URL\"\>forum\<\/a\>\ \|\ \<a href=\"http://web.archive.org/web/*/$forum_URL\"\>archive\<\/a\> >> deleteme2/result
fi

echo >> deleteme2/result


# https://linuxize.com/post/bash-check-if-file-exists/
if [ ! -f deleteme2/result2 ]; then
echo > deleteme2/result2
fi

# https://www.linuxshelltips.com/add-text-to-beginning-of-file-in-linux/
x=`echo \($date_formatted\) NEWS: \[url=\"$news_URL\"\]$title\[/url\] \| \[url=\"$forum_URL\"\]forum thread\[/url\]; cat deleteme2/result2`
echo "$x" > deleteme2/result2

done < "deleteme2/news.csv"


echo
echo Remember to copy contents of deleteme2/news.csv to the main news.csv file!
echo

