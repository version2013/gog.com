#!/bin/sh

# 2021 Oct 01
# 2022 Jan 25
#  It appears gog retroactively added language codes in the news URLs; english has "/en"
#  Added code that removes "/en" from script output.
# 2022 Aug 05
#  concerning this URL: https://www.gog.com/en/news/join_our_bear_and_breakfast_contest
#  news article has more than one instance of forum url.
#  Added code that keeps only first occurance of forum url, thus avoiding script failure.
# 2022 Aug 13
#  Added code to check for http and https
#  Edited code to be less repetitive.
# 2023 Oct 12
#  Added code to check for archive.org URL containing "mp_".


# run script with URL from gog.com or archive.org (with or without "/en")
# examples:
# ./gog_to_csv.sh https://www.gog.com/news/release_myst
# ./gog_to_csv.sh https://www.gog.com/en/news/release_myst
# ./gog_to_csv.sh https://web.archive.org/web/20210915121403/https://www.gog.com/news/release_myst




# https://www.linuxjournal.com/
# https://www.shellscript.sh/variables1.html
# https://www.grymoire.com/Unix/Sed.html

if [ ! -d deleteme2 ]; then
mkdir deleteme2
fi

wget --output-document=deleteme2/news ${1}

# https://tecadmin.net/tutorial/bash/examples/check-if-string-contains-another-string/
# https://devconnected.com/bash-if-else-syntax-with-examples/


if [[ ${1} = *"http:"* ]]; then
echo ${1} | sed 's|http:|https:|g' > deleteme2/step1.csv
else echo ${1} > deleteme2/step1.csv
fi

step1=$(cat deleteme2/step1.csv)

if [[ "$step1" = *"/en"* ]]; then
echo "$step1" | sed 's|/en||' > deleteme2/step2.csv
else echo "$step1" > deleteme2/step2.csv
fi

step2=$(cat deleteme2/step2.csv)

if [[ "$step2" = *"web.archive.org"* ]]; then
   if [[ "$step2" = *"mp_/"* ]]; then
   echo "$step2" | sed 's|https://web.archive.org/web/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]mp_/||' > deleteme2/news_URL.csv
   else echo "$step2" | sed 's|https://web.archive.org/web/[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]/||' > deleteme2/news_URL.csv
   fi
else echo "$step2" > deleteme2/news_URL.csv
fi

news_URL=$(cat deleteme2/news_URL.csv)

echo "$news_URL" | sed 's|https://www.gog.com/news/||' > deleteme2/short_URL.csv

short_URL=$(cat deleteme2/short_URL.csv)

cat deleteme2/news | grep "title article__date" | grep --only-matching datetime=\"[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9] | sed 's/datetime=\"//' > deleteme2/date.csv
sed 's/$/|/' deleteme2/date.csv > deleteme2/data.csv
sed 's|$|'"$news_URL"'|' deleteme2/data.csv > deleteme2/data2.csv
sed 's/$/|/' deleteme2/data2.csv > deleteme2/data3.csv

cat deleteme2/news | grep --only-matching https://www.gog.com/forum/general/"$short_URL"_[a-z0-9][a-z0-9][a-z0-9][a-z0-9][a-z0-9] > deleteme2/forum.csv

# https://unix.stackexchange.com/a/260507
# only keep the first line of forum.csv. Some news articles have more than one instance of forum url, thus messing up this script.
sed -i -n '1p' deleteme2/forum.csv

forum_URL=$(cat deleteme2/forum.csv)
sed 's|$|'"$forum_URL"'|' deleteme2/data3.csv > deleteme2/data4.csv
sed 's/$/|/' deleteme2/data4.csv > deleteme2/data5.csv

cat deleteme2/news | grep " - GOG.com" > deleteme2/title_temp1.csv
sed 's|        ||' deleteme2/title_temp1.csv > deleteme2/title_temp2.csv
sed 's| - GOG.com||' deleteme2/title_temp2.csv > deleteme2/title.csv
echo | gawk -v title="$(gawk '{print}' deleteme2/title.csv)" '{print $0 title}' deleteme2/data5.csv >> deleteme2/news.csv

